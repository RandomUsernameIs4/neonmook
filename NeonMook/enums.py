from enum import Enum

class Attribute(Enum):
    NONE='None'
    BRAWN='Brawn'
    FINESSE='Finesse'
    AGILITY='Agility'
    WITS='Wits'
    RESOLVE='Resolve'
    PRESENCE='Presence'
    EDGE='Edge'
    MAGIC='Magic'
    CYBERWARE_TOLERANCE='Cyberware Tolerance'

class GearCategory(Enum):
    OTHER='Other'
    ARMOR='Armor'
    RANGED='Ranged'
    MELEE='Melee'

class TalentCategory(Enum):
    NON_COMBAT='Non combat'
    COMBAT_GENERAL='Combat'
    DEFENCE='Defence'
    GUNS_GENERAL='Guns'
    PISTOL='Pistol'
    SMG='SMG'
    ASSAULT_RIFLE='Assault Rifle'
    SHOTGUN='Shotgun'
    MELEE_GENERAL='Melee'
    KNIFE='Knife'
    AXE='Axe'
    SWORD='Sword'
    POLEARM='Polearm'
    CLUB='Club'
    SPELLCASTING='Spellcasting'

