from django.contrib import admin

from .models import Gear, Weapon

admin.site.register(Gear)
admin.site.register(Weapon)

