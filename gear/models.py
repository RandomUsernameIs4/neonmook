from django.db import models

from NeonMook.enums import Attribute, GearCategory, TalentCategory
from skills.models import Skill
from talents.models import Talent

class Gear(models.Model):
    used_skill=models.ForeignKey(Skill, on_delete=models.SET_NULL, default=None, blank=True, null=True)
    min_rating=models.IntegerField()
    max_rating=models.IntegerField()
    description=models.TextField()
    name=models.CharField(max_length=20)
    def __str__(self):
        return(self.name)

class Weapon(Gear):
    talent_category=models.CharField(max_length=20, choices=[
        (TalentCategory.NON_COMBAT.name, TalentCategory.NON_COMBAT.value),
        (TalentCategory.COMBAT_GENERAL.name, TalentCategory.COMBAT_GENERAL.value),
        (TalentCategory.DEFENCE.name, TalentCategory.DEFENCE.value),
        (TalentCategory.GUNS_GENERAL.name, TalentCategory.GUNS_GENERAL.value),
        (TalentCategory.PISTOL.name, TalentCategory.PISTOL.value),
        (TalentCategory.SMG.name, TalentCategory.SMG.value),
        (TalentCategory.ASSAULT_RIFLE.name, TalentCategory.ASSAULT_RIFLE.value),
        (TalentCategory.SHOTGUN.name, TalentCategory.SHOTGUN.value),
        (TalentCategory.MELEE_GENERAL.name, TalentCategory.MELEE_GENERAL.value),
        (TalentCategory.KNIFE.name, TalentCategory.KNIFE.value),
        (TalentCategory.AXE.name, TalentCategory.AXE.value),
        (TalentCategory.SWORD.name, TalentCategory.SWORD.value),
        (TalentCategory.POLEARM.name, TalentCategory.POLEARM.value),
        (TalentCategory.CLUB.name, TalentCategory.CLUB.value),
        (TalentCategory.SPELLCASTING.name, TalentCategory.SPELLCASTING.value),
        ])
