from django.shortcuts import render

from .models import Gear

def index(request):
    context={}
    context['gears'] = Gear.objects.all()
    
    return render(request, 'gear.html', context)
