from django.shortcuts import render

from .models import Skill

def index(request):
    context={}
    context['skills'] = Skill.objects.all()
    return render(request, 'skill.html', context)
