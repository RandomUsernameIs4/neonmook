from django.db import models

from NeonMook.enums import Attribute

class Skill(models.Model):
    used_attribute=models.CharField(max_length=10, choices=[
        (Attribute.BRAWN.name, Attribute.BRAWN.value),
        (Attribute.FINESSE.name, Attribute.FINESSE.value),
        (Attribute.FINESSE.name, Attribute.FINESSE.value),
        (Attribute.AGILITY.name, Attribute.AGILITY.value),
        (Attribute.WITS.name, Attribute.WITS.value),
        (Attribute.PRESENCE.name, Attribute.PRESENCE.value),
        (Attribute.MAGIC.name, Attribute.MAGIC.value),
        (Attribute.NONE.name, Attribute.NONE.value)])
    name=models.CharField(max_length=20)
    
    def __str__(self):
        return(self.name)
