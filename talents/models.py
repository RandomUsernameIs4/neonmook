from django.db import models

from NeonMook.enums import TalentCategory

class Talent(models.Model):
    name=models.CharField(max_length=40)
    depends_on=models.ForeignKey('Talent', on_delete=models.CASCADE, default=None, blank=True, null=True)
    category=models.CharField(max_length=20, choices=[
        (TalentCategory.NON_COMBAT.name, TalentCategory.NON_COMBAT.value),
        (TalentCategory.COMBAT_GENERAL.name, TalentCategory.COMBAT_GENERAL.value),
        (TalentCategory.DEFENCE.name, TalentCategory.DEFENCE.value),
        (TalentCategory.GUNS_GENERAL.name, TalentCategory.GUNS_GENERAL.value),
        (TalentCategory.PISTOL.name, TalentCategory.PISTOL.value),
        (TalentCategory.SMG.name, TalentCategory.SMG.value),
        (TalentCategory.ASSAULT_RIFLE.name, TalentCategory.ASSAULT_RIFLE.value),
        (TalentCategory.SHOTGUN.name, TalentCategory.SHOTGUN.value),
        (TalentCategory.MELEE_GENERAL.name, TalentCategory.MELEE_GENERAL.value),
        (TalentCategory.KNIFE.name, TalentCategory.KNIFE.value),
        (TalentCategory.AXE.name, TalentCategory.AXE.value),
        (TalentCategory.SWORD.name, TalentCategory.SWORD.value),
        (TalentCategory.POLEARM.name, TalentCategory.POLEARM.value),
        (TalentCategory.CLUB.name, TalentCategory.CLUB.value),
        (TalentCategory.SPELLCASTING.name, TalentCategory.SPELLCASTING.value),
        ])
    description=models.TextField()
    min_rating=models.IntegerField(default=1)
    max_rating=models.IntegerField(default=20)
    available_to_mooks=models.BooleanField(default=True)

    def __str__(self):
        return(self.name+' ['+TalentCategory[self.category].value+']')
    
