from django.shortcuts import render

from .models import Talent
from NeonMook.enums import TalentCategory

def index(request):
    context={}
    talents={}
    for category in TalentCategory:
        talents[category] = Talent.objects.filter(category=category.name)
    context['talents'] = talents
    
    return render(request, 'talents.html', context)
