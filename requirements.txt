Django==2.2
psycopg2==2.8.3
gunicorn==19.9.0
dj-database-url==0.5.0
whitenoise==4.1.4	
