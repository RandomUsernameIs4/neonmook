FROM python:3
ENV PYTHONUNBUFFERED 1
WORKDIR /NeonMooks
COPY requirements.txt /NeonMooks/
RUN pip install -r requirements.txt
ADD . /NeonMooks/
RUN SECRET_KEY="secret" python manage.py collectstatic --noinput
CMD gunicorn NeonMook.wsgi:application --log-file=-
