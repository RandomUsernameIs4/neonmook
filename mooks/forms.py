from django import forms

class GenerationForm(forms.Form):
    rating = forms.ChoiceField(label='Max dicepool', choices=((str(x), x) for x in range(6,15)))
    can_be_mage_or_shaman = forms.BooleanField(required=False)
