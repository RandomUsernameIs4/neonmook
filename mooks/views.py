from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.db.models import Q

import random

from .forms import GenerationForm
from NeonMook.enums import Attribute, TalentCategory
from skills.models import Skill
from gear.models import Gear, Weapon
from talents.models import Talent

def index(request):
    context={}
    if request.GET:
        form = GenerationForm(request.GET)
        if form.is_valid():
            rating=int(form.cleaned_data['rating'])
            can_be_mage_or_shaman = bool(form.cleaned_data['can_be_mage_or_shaman'])
            average_score=4
            max_score = min(6, int(int(rating)/2))
            pool=max_score*average_score
            numbers=[]
            
            for i in range(0,6):
                if i == 5:
                    number=min(max_score, max(average_score, int(pool-(pool-average_score)/2)))
                    cost=number
                elif int(round(pool/(6-i))) > max_score:
                    number = max_score
                    cost=number+(number-average_score)
                elif i>1 and pool/(6-i) < average_score:
                    number = max(1, int(round(pool/(6-i))))
                    cost=number
                else:
                    number = random.randint(1, max_score)
                    cost=number+(number-average_score)
                pool-=cost
                numbers.append(number)
                
            stats = dict(zip(list(Attribute)[1:], numbers))
            highest_stat = Attribute.FINESSE
            checked_stats = [Attribute.BRAWN, Attribute.FINESSE]
            if can_be_mage_or_shaman:
                checked_stats += [Attribute.WITS, Attribute.PRESENCE]
            for stat in checked_stats:
                if stats[stat]>stats[highest_stat]:
                    highest_stat = stat

            stats[Attribute.EDGE] = 0
            if highest_stat == Attribute.WITS or highest_stat == Attribute.PRESENCE:
                stats[Attribute.MAGIC] = min(6, int(int(rating)/2))
            else:
                stats[Attribute.MAGIC] = 0
            stats[Attribute.CYBERWARE_TOLERANCE] = 0

            context['main_stat'] = highest_stat
            context['generated_values'] = stats

            skills={}
            main_weapon = None
            if highest_stat == Attribute.BRAWN:
                main_skill = Skill.objects.get(name='Melee')
                secondary_skill = Skill.objects.get(name='Firearms')
            elif highest_stat == Attribute.FINESSE:
                main_skill = Skill.objects.get(name='Firearms')
                secondary_skill = Skill.objects.get(name='Melee')
            else:
                main_skill = Skill.objects.get(name='Spellcasting')
                secondary_skill = Skill.objects.get(name='Firearms')
            weapons = Weapon.objects.filter(used_skill=main_skill).filter(min_rating__lte=rating).filter(max_rating__gte=rating)
            if weapons.count():
                main_weapon = weapons[random.randint(0, weapons.count()-1)]
                context['main_weapon'] = main_weapon

            secondary_weapon = None
            if rating>8 and highest_stat in [Attribute.BRAWN, Attribute.FINESSE] or highest_stat in [Attribute.WITS, Attribute.PRESENCE]:
                weapons = Weapon.objects.filter(used_skill=secondary_skill).filter(min_rating__lte=rating).filter(max_rating__gte=rating)
                if weapons.count():
                    secondary_weapon = weapons[random.randint(0, weapons.count()-1)]
                    context['secondary_weapon'] = secondary_weapon
            if highest_stat in [Attribute.WITS, Attribute.PRESENCE]:
                skills[main_skill] = int(rating)-stats[Attribute.MAGIC]
            else:
                skills[main_skill] = int(rating)-stats[highest_stat]
            skills[secondary_skill] = int(int(rating)/3)
            context['skills'] = skills

            talents=[]
            number_of_talents = int(min(12, rating)/2)
            while len(talents) < number_of_talents:
                talent = None
                while talent is None or talent in talents:
                    talent_set = Talent.objects.filter(available_to_mooks=True).filter(min_rating__lte=rating).filter(max_rating__gte=rating)
                    if len(talents) < int(number_of_talents/2) or (main_skill == Skill.objects.get(name='Spellcasting') and i<int(number_of_talents*3/4)):
                        if main_weapon:
                            talent_set=talent_set.filter(category=main_weapon.talent_category)
                        else:
                            talent_set=talent_set.filter(category=TalentCategory.SPELLCASTING.name)
                    elif len(talents)<int(number_of_talents*3/4) and secondary_weapon:
                        talent_set=talent_set.filter(category=secondary_weapon.talent_category)
                    else:
                        talent_set=talent_set.filter(Q(category=TalentCategory.DEFENCE.name) | Q(category=TalentCategory.COMBAT_GENERAL.name))
                    talent = talent_set[random.randint(0, talent_set.count()-1)]
                    if talent_set.count() < number_of_talents - len(talents):
                        break
                talent_with_parents = []
                while talent:
                    if not talent in talents:
                        talent_with_parents.append(talent)
                    talent = talent.depends_on
                talent_with_parents.reverse()
                if len(talent_with_parents)+len(talents)>number_of_talents:
                    talent_with_parents=talent_with_parents[0:number_of_talents-len(talents)]
                talents.extend(talent_with_parents)
            context['talents'] = talents
        context['generation_form'] = form
    else:
        context['generation_form'] = GenerationForm()
    return render(request, 'mook.html', context)
