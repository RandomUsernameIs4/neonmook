from django.apps import AppConfig


class MooksConfig(AppConfig):
    name = 'mooks'
